import random

list_de_personnes = []

for x in range(10):
    personne = {
    "Sexe" : "".join(random.choices(["M","F"])),
    "Age" : random.randint(6, 65),
    "Poids" : random.randint(20, 100),
    "Taille" : random.randint(120, 200)/100
    }
    list_de_personnes.append(personne)

for x in list_de_personnes:
    IMC = round(x["Poids"] / x["Taille"]**2)
    x["IMC"] = IMC
    
    for cle, valeur in x.items():
        print(f"{cle} : {valeur}")
    
    print("\n_________________________________________________\n")