import random

# J'importe la librairie "random" pour pouvoir generer aleatoirement des valeurs

list = []

x = 1

for x in range(10):
    tuple = (random.randint(6, 65), random.randint(20, 100), random.randint(120, 200)/100, "".join(random.choices(["M","F"])))
    list.append(tuple)

# Je cree un tuple de 3 elements generer aleatoirement puis je l'ajoute a ma liste, tout ca 10 fois

for x in list:

    # Je parcours les tuples de ma liste afin d'en recuperer les elements qui m'interesse comme l'age, le poids ou la taille

    if x[0] > 20:

        imc = round(x[1] / x[2]**2)

        print(f"Sexe : {x[3]}\nAge : {x[0]}ans\nPoids : {x[1]}kg\nTaille : {x[2]}m")

        # J'applique la formule.

        if float(imc) <= 18.5:
            print(f"IMC = {imc} : maigre")
        elif float(imc) > 18.5 and imc <= 25:
            print(f"IMC = {imc} : normal")
        elif float(imc) > 25 and imc <= 30:
            print(f"IMC = {imc} : surpoids")
        elif float(imc) > 30 and imc <= 35:
            print(f"IMC = {imc} : modérée")
        elif float(imc) > 40 and imc <= 45:
            print(f"IMC = {imc} : sévère")
        elif float(imc) >= 45:
            print(f"IMC = {imc} : morbide")
        
        print("\n______________________________________________________________\n")

    else:
        print("Tu es trop jeune pour te demander si tu es gros")
        print("\n______________________________________________________________\n")

    # Je renvois le résultat de mon calcul avec le commentaire correspondant a l'IMC de la personne.