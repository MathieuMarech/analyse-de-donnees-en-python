import numpy

array_age = numpy.random.randint(20, 65,size=10)
array_nom = numpy.random.choice(["Mathieu","Isaac","Louis","Abdelaziz","Antoine","Lili","Clémence","Véronique","Severine","Fatima"],size=10)
array_sexe = numpy.random.choice(["M","F"], size=10)
array_height = numpy.random.randint(120, 200,size=10)/100
array_weight = numpy.random.randint(20, 120,size=10)

IMC = numpy.round(array_weight / array_height ** 2, 1)

data = numpy.array([array_age, array_nom, array_sexe, array_height, array_weight, IMC])

data = data.T

numpy.savetxt("data.csv", data, delimiter=" | ", header="Age | Nom | Sexe | Height | Weight | IMC", fmt='%s')

#Valentin Mougin